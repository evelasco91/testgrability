public function post_confirm(){
    
	$id = Input::get('service_id');
    $servicio = Service::find($id);
	
    if ($servicio != NULL){
        
		if  ($servicio->status_id == '6'){
            
			return Response::json(array('error' => '2'));
        }
		// Validation for search any driver with the service are 1
        if ($servicio->driver_id == NULL && $servicio->status_id == '1'){
            
			$idDriver	=	Input::get('driver_id');
			$driverTmp 	= 	Driver::find($idDriver);
			
			$servicio 	= 	Service::update($id, array(
                'driver_id' => $idDriver ,
                'status_id' => '2'    
            ));
            
			Driver::update($idDriver, array(
                'available' => '0'
            ));
			
            Service::update($id, array(
                'car_id'=>$driverTmp->car_id
               
            ));
            //Notifier the user 
            $pushMessage = 'Tu servicio ha sido confirmado!';
            $servicio = Service::find($id);
            $push = Push::make();
			
			// Error with the uuif is empty
            if ($servicio->user->uuid == ''){
                
				return Response::json(array('error'=>'0'));
				
            }
            // Type 1 =  Iphone, type 2 = Android
			if($servicio->user->type == '1'){
                
				$result = $push->ios($servicio->user->uuid, $pushMessage, 1, 'honk.wav',
                                        'Open', array('service_id'=>$servicio->id));
            } else{
                
				$result = $push->android2($servicio->user->uuid, $pushMessage, 1, 'default',
                                        'Open', array('service_id'=>$servicio->id));
            }
            
			return Response::json(array('error'=>'0'));
			
        }else{
            
			return Response::json(array('error'=>'1'));
        }
		
    }
	else{
		
        return Response::json(array('error'=>'3'));
    }
}